import os

import pytest
import vcr

import sdvclient
from sdvclient import client as client_
from sdvclient import models
from .utils import sdvclientvcr


def test_top_level_import():
    assert sdvclient.Client == client_.Client


class TestClient:
    @pytest.fixture
    def client(self):
        return sdvclient.Client()

    def test_init(self, monkeypatch):
        client = sdvclient.Client(access_token="some_token")
        assert client.access_token == "some_token"

        monkeypatch.setenv("SDV_AUTH_TOKEN", "some_token")
        client = sdvclient.Client()
        assert client.access_token == "some_token"

    def test_client_no_access_token(self, client, monkeypatch):
        monkeypatch.delenv("SDV_AUTH_TOKEN", raising=False)
        with pytest.raises(ValueError):
            client_.Client()

    @sdvclientvcr.use_cassette()
    def test_my_data(self, client):
        num = 0
        for dataset in client.my_data(limit=50):
            assert isinstance(dataset, models.DatasetSummary)
            num += 1

        assert num == 50

    @sdvclientvcr.use_cassette()
    def test_get_data_strava(self, client):
        dataset = client.get_data(id=30430)

        assert isinstance(dataset, models.Dataset)
        assert dataset.sport == "sports.workout"
        assert dataset.tags == ["sports.workout", "tags.strava"]
        assert dataset.title == "Evening Activity"
        assert dataset.data.metadata["average_heartrate"] == 125.7

    @sdvclientvcr.use_cassette()
    def test_get_data_questionnaire(self, client):
        dataset = client.get_data(id=30329)

        assert isinstance(dataset, models.Dataset)
        assert dataset.sport == "sports.questionnaire"
        assert dataset.tags == ["sports.questionnaire", "tags.vragenlijst"]
        assert dataset.title == "Training log"
        assert len(dataset.data.questions) == 16
        assert dataset.data.questions[4].question == "v4_minuten"
        assert dataset.data.questions[4].answer == "0"
