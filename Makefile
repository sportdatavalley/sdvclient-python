.PHONY: build_docker build_test test testall docs

build_docker:
	docker build -t sdvclient-test .

test:
	docker-compose -f docker/docker-compose.test.yml build
	docker-compose -f docker/docker-compose.test.yml run sdvclient tox -- ${pytestargs}

lint:
	docker-compose -f docker/docker-compose.lint.yml build
	docker-compose -f docker/docker-compose.lint.yml run sdvclient

testall:
	docker-compose -f docker/docker-compose.test.yml build
	docker-compose -f docker/docker-compose.test.yml run sdvclient

docs:
	docker-compose -f docker/docker-compose.docs.yml up --remove-orphans
