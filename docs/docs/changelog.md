# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

The same types of changes should be grouped.
Types of changes:

- `Added` for new features.
- `Changed` for changes in existing functionality.
- `Deprecated` for soon-to-be removed features.
- `Removed` for now removed features.
- `Fixed` for any bug fixes.
- `Security` in case of vulnerabilities.

## [Unreleased] - {ISO8601 date}

## [0.5.2] - 2021-7-2
### Changed
- Checks availability before accessing structured_data_objects.
- Caches of group data, and searches the cache before sending requesting single datasets.

## [0.5.1] - 2021-02-12
### Fixed
- Fixed the name of the field for retrieving the vdo's from the JSON response. (`"data"` instead of `"vdo"`)

## [0.5.0] - 2020-12-15
### Added
- Adds `sdv.connections()` method.
- Adds `sdv.groups()` method.
- Adds `sdv.connection_datasets()` method.
- Adds `sdv.group_datasets()` method.


## [0.4.1] - 2020-12-14
### Added
- Bring back top-level `get_data()` method.


## [0.4.0] - 2020-12-14
### Changed
- Renamed `my_data()` to `my_datasets()`.
- Renamed `network_data()` to `network_datasets()`.
- Added `get_data()` method to DatasetSummary.

### Removed
- Removed `get_data()` method in favor of method on `DatasetSummary` model.


## [0.3.0] - 2020-11-24
### Added
- Support for unstructured and unsupported data types.

### Fixed
- Fixed reference to `sdv.my_dataset()` in docs to `sdv.get_dataset()`.


## [0.2.0] - 2020-11-24
### Added
- Support for questionnaires, sdv data format and daily activity data

### Changes
- Moved away from object oriented client


## [0.1.0] - 2020-07-09
### Added
- Project init
- Client().my_data() implementation
- DatasetSummary model
- Client().get_data() implementation
- Support for questionnaire and SDV format
